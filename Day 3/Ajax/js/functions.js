$(document).ready(function() {
    // Fetch data
    $('#fetch-data').on('click', function() {
        var root = 'http://jsonplaceholder.typicode.com';

        $.ajax({
            url: root + '/users',
            method: 'GET'
        }).then(function(data) {
            // Clear the old data
            $('#users-table tbody tr').remove();

            // Put the new data in the table
            var table = $("#users-table tbody");
            $.each(data, function(idx, elem) {
                table.append(
                    "<tr>" +
                    "<td>" + elem.id + "</td>" +
                    "<td>" + elem.name + "</td>" +
                    "<td>" + elem.username + "</td>" +
                    "<td>" + elem.email + "</td>" +
                    "<td>" + elem.address.city + "</td>" +
                    "<td>" + elem.phone + "</td>" +
                    "<td>" + elem.website + "</td>" +
                    "<td>" + elem.company.name + "</td>" +
                    "</tr>");
            });
        });
    });
});
