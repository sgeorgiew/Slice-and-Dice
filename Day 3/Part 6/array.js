var bucketList = ['Rappel into a cave', 'Take a falconry class', 'Learn to juggle'];
console.log(bucketList);

var listItem = bucketList[0];
console.log(listItem); // Output: 'Rappel into a cave'

listItem = bucketList[2];
console.log(listItem); // Output: 'Learn to juggle'

var bucketListLength = bucketList.length;
console.log(bucketListLength); // Output: 3

bucketList.push('Item 4', 'New item');
console.log(bucketList);

bucketList.pop();
console.log(bucketList);
